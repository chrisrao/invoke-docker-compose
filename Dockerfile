FROM docker:20.10.8-alpine3.13

MAINTAINER chris.va.rao@gmail.com

RUN apk add --update rust cargo python3 python3-dev py3-pip gcc libc-dev libffi-dev openssl-dev make bash

RUN pip3 install invoke docker-compose
